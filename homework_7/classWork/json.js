
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

*/

var form = document.forms.myForm;

form.addEventListener('submit', function (e) {
	var obj = {};
	e.preventDefault();
	var formElems = Array.from(form);
	formElems.forEach(function (el) {
		if (el.localName === 'input') {
			obj[el.name] = el.value;
		}

	})
	var jsonString = JSON.stringify(obj);
	console.log('jsonString', jsonString);
})

var parseForm = document.forms.myParse;
console.log(parseForm);

parseForm.addEventListener('submit', function (e) {
	e.preventDefault();
	var parseInput = parseForm.parseInput;

	var string = parseInput.value;
	var obj = JSON.parse( string );

	console.log(obj);

})