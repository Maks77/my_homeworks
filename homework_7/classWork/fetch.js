/*
	Задача:

	1. При помощи fetch получить данные:
		 http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

	2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
	3. Выбрать случайного человека и передать в следующий чейн промиса
	4. Сделать дополнительный запрос на получение списка друзей человека
		 http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
		 И дальше передать обьект:
			{
				name: userName,
				...
				friends: friendsData
			}

		 Подсказка нужно вызвать дополнительный fecth из текущего чейна.
		 Для того что бы передать результат выполнения доп. запроса
		 в наш первый промис используйте констуркцию:

			.then(
				response1 => {
					return fetch(...)
						.then(
							response2 => {
								...
								формируете обьект в котором будут данные человека с
								первого запроса, например его name response1.name
								и друзья которые пришли из доп. запроса
							}
						)
				}
			)

	5. Вывести результат на страничку

	+ Бонус. Для конвертации обьекта response в json использовать одну
		функцию.

*/
var myUrl = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
var secondUrl = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';

fetch( myUrl )
	.then( function( res ){
		return res.json();
	}).then( function( res ){
		var user = res[ getRandom(0, res.length) ];
		console.log( 'user:', user );
		return user;
	}).then( function ( user ) {

		var targetUser = {};
			targetUser.name = user.name;

		fetch( secondUrl )
			.then( function (res) {

				console.log('second', res );
				return res.json();

			}).then( function ( res1 ) {
				
				console.log('res1', res1);
				targetUser.friends = res1[0].friends;
				console.log('targetUser', targetUser)

			})
	})



function getRandom(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}
