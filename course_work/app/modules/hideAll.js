function hideAll(arr, itemIdent, removeClss) {
	arr.forEach(el=>{
		let item = el.querySelector(itemIdent);
		item.classList.remove(removeClss);
	})
}

export default hideAll;