class Display {
	constructor() {
		this.form = document.forms.displayForm;
		this.id = 1;
		this.initSubmitEvent();
	}

	initSubmitEvent() {
		this.form.addEventListener('submit', event => {
			event.preventDefault();
			this.openModalWin();
			let infoTarget = document.getElementById('modalContent');
				infoTarget.innerHTML = null;
			let existedElements = this.getElements();
				existedElements.forEach(el=>{

					let item = el.children[1];
					
					let iValue;
					if (item.type == 'checkbox' || item.type == 'radio') {
						iValue = item.checked;
					} else {
						iValue = item.value;
					}

					let iName = item.name;
					let iType = el.dataset.type;


					let infoBlock = document.createElement('li');
						infoBlock.className = 'modalItem';
					let pType = document.createElement('p');
						pType.innerHTML = `type of element: <span class='modText-1'> ${iType}</span>`;
					let pName = document.createElement('p');
						pName.innerHTML = `name of element: <span class='modText-1'> ${iName}</span>`;
					let pValue = document.createElement('p');
						pValue.innerHTML = `value: <span class='modText-2'>${iValue}</span`;

					infoBlock.appendChild(pType);
					infoBlock.appendChild(pName);
					infoBlock.appendChild(pValue);

					infoTarget.appendChild(infoBlock);
				})
		})
	}

	openModalWin() {
		let wrap = document.getElementById('modal_wrap');
			wrap.classList.add('modal_show');
			wrap.style.display = 'block';

		let closeBtn = wrap.querySelector('.close');

		closeBtn.addEventListener('click', e=>{
			wrap.style.display = 'none';
		});
	}

	createElem( name, type ) {
		let thisname = (name === 'default') ? (name = name + this.id) : name;

		let label = document.createElement('label');
			label.className = 'displayLabel';
			label.id = this.id;
			label.setAttribute('data-type', type);
			label.setAttribute('data-name', name);
			label.innerHTML = `<span style="color: #2A6E2A"> element "${type}"; name: ${name}</span>`;
		let elem;

		if (type === 'textarea') {
			elem = document.createElement('textarea');
			elem.setAttribute('name', thisname);
			elem.className = 'forDisplay';
		} else {
			elem = document.createElement('input');
			elem.setAttribute('name', thisname);
			elem.setAttribute('type', type);
			elem.className = 'forDisplay';
		}
		
		label.appendChild(elem);
		this.form.appendChild( label );
		this.id++;
	}

	getElements() {
		return Array.from(this.form.children);
	}

}

export default Display;