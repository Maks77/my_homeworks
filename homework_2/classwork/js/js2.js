var btnHover = document.getElementById('mouseHover');
var hoverResult = document.getElementById('hoverResult');

function getRandom(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}

btnHover.onmouseenter = function () {
	var colors = ['red', 'blue', 'green', 'orange', 'purple'];
	var random = getRandom(0, colors.length - 1);
	var li = document.createElement('li');
		li.innerText = 'hovered!';
		li.className = colors[random];
		console.log(li);

	hoverResult.appendChild(li);
}

//==============================================================================
var btnClick = document.getElementById('mouseClick');
var clickResult = document.getElementById('clickResult');
var count = 0;
var removeTrigger = true;
btnClick.onclick = function () {
	var removeArray = ['six', 'two', 'three', 'four', 'one', 'five'];
	var newArray = ['one', 'two', 'three', 'four', 'five', 'six'];

	console.log( removeTrigger );
	console.log( clickResult.classList.length );
	
	if (clickResult.classList.length === 1) {
		count = 0;
		removeTrigger = false;
	} else if (clickResult.classList.length > 6) {
		count = 0;
		removeTrigger = true;
	}


	if (removeTrigger) {
		clickResult.classList.remove(removeArray[count]);
		count++;
	} else {
		clickResult.classList.add(newArray[count]);
		count++;
	}
}


//==============================================================================

var btnContext = document.getElementById('mouseContext');
var contextResult = document.getElementById('contextResult');
var contextClicked = false;
btnContext.oncontextmenu = function (e) {
	e.preventDefault();
	if (contextClicked) {
		console.dir(contextResult);
		contextResult.innerHTML = null;
		contextClicked = false;
	} else {

		var ul = document.createElement('ul');

		var google = document.createElement('a');
			google.setAttribute('href', 'http://google.com.ua');
			google.innerText = 'Google';
		var itea = document.createElement('a');
			itea.setAttribute('href', 'http://itea.ua');
			itea.innerText = 'ITEA';
		var youtube = document.createElement('a');
			youtube.setAttribute('href', 'https://www.youtube.com/');
			youtube.innerText = 'Youtube';

		var li = document.createElement('li');
			li.appendChild(google);
			ul.appendChild(li);
		var li = document.createElement('li');
			li.appendChild(itea);
			ul.appendChild(li);
		var li = document.createElement('li');
			li.appendChild(youtube);
			ul.appendChild(li);


			contextResult.appendChild(ul);
			contextClicked = true;
	}
}

//==============================================================================
var btnKeyboard = document.getElementById('keyboardButton');
var keyboardResult = document.getElementById('keyboardResult');

var count = 0;
btnKeyboard.onkeydown = function () {
	count++;
	keyboardResult.innerText = count;
}