
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */


var buttonContainer = document.getElementById('buttonContainer');
var tabs = document.querySelectorAll('.tab');
var lastClick;

buttonContainer.onclick = function (event) {

    if (event.target.type === 'button') {
        var tabIndex = event.target.dataset.tab;
        console.log(tabIndex);
        if (lastClick === tabIndex) {
            hideAllTabs();
            return;
        }
        tabs.forEach(function (el) {
            console.log(el);

            if (tabIndex === el.dataset.tab) {
                var isActive = el.classList.contains('active');
                el.classList.add('active');
                console.log('isActive', isActive);
            } else {
                el.classList.remove('active');
            }
        })
        lastClick = tabIndex;
    }
}

function hideAllTabs() {
    tabs.forEach(function (el) {
        el.classList.remove('active');
    });
}


//
