
function getRandom(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}


var title = document.createElement('h1');
	title.style.textAlign = 'center';
	title.style.marginTop = '100px';

var btn = document.createElement('button');
	btn.id = "button";
	btn.innerText = "Click on me";
	btn.style.width = '300px';
	btn.style.height = '100px';
	btn.style.fontSize = '30px';
	btn.style.display = 'block';
	btn.style.margin = '20px auto';
	


function setRandomColor() {
	var colors = [];

	for (var i = 0; i < 3; i++) {
		var random = getRandom( 0,255 );
		colors.push(random.toString( 16 ))
	}

	var bgColor = '#' + colors.join('');

	document.body.style.background = bgColor;
	title.innerText = '==> ' + bgColor.toUpperCase() + ' <==';
	
}

window.onload = function () {
	document.body.appendChild(title);
	document.body.appendChild(btn);
	setRandomColor();
}

document.onclick = function (e) {
	if (e.target.id === 'button') {
		setRandomColor()
	}
}