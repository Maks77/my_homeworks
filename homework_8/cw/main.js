// (\b\w+)
// (a.+?e)
// [0-9]{4}
// \[\w+\]

// \.\w{3}\b //?
// (?!\.com)\.\w+

// (\b\w*)@(\w*\.\w*)
// [A-Z]\w*\b



function setRandomColor() {

	function getRandom(min, max) {
		return Math.floor(Math.random() * (max - min) + min);
	}
	var colors = [];

	for (var i = 0; i < 3; i++) {
		var random = getRandom( 0,255 );
		colors.push(random.toString( 16 ))
	}

	var bgColor = '#' + colors.join('');

	return bgColor;
	
}


let bgColor;

let btn = document.getElementById('btn');
	btn.addEventListener('click', e => {
		var color = setRandomColor();
		console.log(color);
		localStorage.setItem('color', color);
		bgColor = color;
		document.body.style.background = bgColor;
	})

window.addEventListener('load', e => {
	let res = document.getElementById('res');
	let mColor = localStorage.getItem("color");
	res.innerText = mColor;
	document.body.style.background = mColor;
})