/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/

function Dog(name, strain) {
    this.name = name;
    this.strain = strain;

    this.move = function () {
        console.log(this.name + " " + 'is running');
    };

    this.eat = function () {
        console.log('I am eating...');
    }
}

var dog = new Dog('Sharik', 'buldog');

dog.move();
dog.eat();

for (let key in dog) {
    console.log(key);
}