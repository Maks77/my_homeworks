/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/


var Train = {
	name: 'intercity',
	speed: 200,
	passangersAmount: 200,

	move: function () {
		console.log(`Поезд ${this.name} везет ${this.passangersAmount} пассажиров со скоростью ${this.speed}`);
	},

	stop: function () {
		console.log(`Поезд ${this.name} остановился. Скорость 0`)
	},

	takePass: function (amount) {
		console.log(this.speed + amount);
	}
}

Train.move();
Train.stop();
Train.takePass(60);