/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент, второй попадает в него через метод .call(obj)
    1.2 Ф-я не принимает никаких аргументов, а необходимые настройки полностью передаются через bind
    1.3 Ф-я принимает фразу для заголовка, обьект с настройками передаем через .apply();

*/

var obj = {
	bgColor : 'red',
	textColor : 'yellow',
}

//1.0 Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
function changeColor0(bgColor, textColor) {
	document.body.style.backgroundColor = bgColor;
	document.body.style.color = textColor;
	let title = document.createElement('h1');
		title.innerText = "I know how binding works in JS";

	document.body.appendChild(title);

}

// changeColor0('red', 'black');


//1.1 Ф-я принимает один аргумент, второй попадает в него через метод .call(obj)
function changeColor1(bgColor) {
	document.body.style.backgroundColor = bgColor;
	document.body.style.color = this.textColor;
	let title = document.createElement('h1');
		title.innerText = "I know how binding works in JS";

	document.body.appendChild(title);

}

// changeColor1.call(obj, 'red');


//1.2 Ф-я не принимает никаких аргументов, а необходимые настройки полностью передаются через bind
function changeColor2() {
	document.body.style.backgroundColor = this.bgColor;
	document.body.style.color = this.textColor;
	let title = document.createElement('h1');
		title.innerText = "I know how binding works in JS";

	document.body.appendChild(title);

}

// changeColor2.bind(obj)();


//1.3 Ф-я принимает фразу для заголовка, обьект с настройками передаем через .apply();
function changeColor3( text ) {
	document.body.style.backgroundColor = this.bgColor;
	document.body.style.color = this.textColor;
	let title = document.createElement('h1');
		title.innerText = text;

	document.body.appendChild(title);
}


changeColor3.apply(obj, ['Hello! I`m a advanced JS coder']);
