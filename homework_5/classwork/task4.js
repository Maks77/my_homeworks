/*

	Задание "Шифр цезаря":

		https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

		Написать функцию, которая будет принимать в себя слово и количество
		симовлов на которые нужно сделать сдвиг внутри.

		Написать функцию дешефратор которая вернет слово в изначальный вид.

		Сделать статичные функции используя bind и метод частичного
		вызова функции (каррирования), которая будет создавать и дешефровать
		слова с статично забитым шагом от одного до 5.

		Например:
			encryptCesar('Word', 3);
			encryptCesar1(...)
			...
			encryptCesar5(...)

			decryptCesar1('Sdwq', 3);
			decryptCesar1(...)
			...
			decryptCesar5(...)

*/

// console.log(String.fromCharCode(1040, 1041, 1042));
const startPos = 97;
const endPos = 122;

function encryptCesar(word, shift) {
	let arr = [];
	
	for (let i = 0; i < word.length; i++) {
		var item = word.charCodeAt(i);
		if ( item + shift > endPos ) {
			var difference = endPos - item;
			var startShift = shift - difference;
			arr.push( String.fromCharCode(startPos + startShift - 1) );
		} else {
			arr.push( String.fromCharCode(item + shift) );
		}
		
		
	}
	return arr.join('');
}

function decryptCesar(word, shift) {
	let arr = [];
	for (let i = 0; i < word.length; i++) {
		var item = word.charCodeAt(i);
		if ( item - shift < startPos ) {
			var difference = item - startPos;
			// console.log(difference);
			var endShift = shift - difference - 1;
			console.log(endShift);
			console.log(endPos - endShift - 1);
			arr.push( String.fromCharCode('endPos - endShift', endPos - endShift) );
		} else {
			arr.push( String.fromCharCode(item - shift) );
		}
	}
	return arr.join('');
}

var res = encryptCesar('zzz', 2);
console.log('encrypt', res);
var res1 = decryptCesar('ggg', 2);
console.log('decrypt', res1);


