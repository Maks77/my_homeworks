/*

	Задание:

		1. Написать конструктор объекта комментария который принимает 3 аргумента
		( имя, текст сообщения, ссылка на аватаку);

		{
			name: '',
			text: '',
			avatarUrl: '...jpg'
			likes: 0
		}
			+ Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
			+ В прототипе должен быть метод который увеличивает счетик лайков

		var myComment1 = new Comment(...);

		2. Создать массив из 4х комментариев.
		var CommentsArray = [myComment1, myComment2...]

		3. Созадть функцию конструктор, которая принимает массив коментариев.
			И выводит каждый из них на страничку.

		<div id="CommentsFeed"></div>

*/

//http://simpleicon.com/wp-content/uploads/user1.png



var someText = [
	'Lorem ipsum dolor sit.',
	'Lorem ipsum dolor.',
	'Lorem ipsum dolor sit amet.',
	'Lorem ipsum.'
];

var someName = [
	'John',
	'Steve',
	'Michael',
	'Vlad'
];

function Comment( name, msgText, avatarUrl ) {
	this.name = name;
	this.msgText = msgText;
	if( avatarUrl !== undefined){
		this.avatarUrl = avatarUrl;
	}
	
	this.likes = 0;
}

Comment.prototype.avatarUrl = 'http://simpleicon.com/wp-content/uploads/user1.png';
Comment.prototype.addLike = function () {
	this.likes++;
}

var commentsArray = [];
for (let i = 0; i < 4; i++) {
	let commentItem;
	
	commentItem = new Comment ( someName[i], someText[i] );	
	commentsArray.push( commentItem );
}

console.dir(commentsArray);

function render( obj ) {
	var dest = document.getElementById('CommentsFeed');

	var commentBlock = document.createElement('div');
		commentBlock.className = 'commentItem';
	var avatarImg = document.createElement('img');
		avatarImg.setAttribute('src', obj.avatarUrl);
		avatarImg.className = 'avatar';
	var commentName = document.createElement('div');
		commentName.innerText = obj.name;
	var commentText = document.createElement('div');
		commentText.innerText = obj.msgText;
		commentText.className = 'msgText';
	var likeBtn = document.createElement('div');
		likeBtn.className = 'likeBtn';

		// obj.addLike = obj.addLike.bind(obj)
		// likeBtn.addEventListener('click', obj.addLike)

	var likeCount = document.createElement('div');
		likeCount.className = 'likeCount';
		likeCount.innerText = obj.likes;


	
	commentBlock.appendChild( avatarImg );
	commentBlock.appendChild( commentName );
	commentBlock.appendChild( commentText );
	commentBlock.appendChild( likeBtn );
	commentBlock.appendChild( likeCount );

	
	dest.appendChild(commentBlock);
	return commentBlock;
}

commentsArray.forEach(el => {
	var item = render(el);

		item.addLike = el.addLike.bind(el);
		item.addEventListener('click', event => {
			handler(item, el, event);
		})

})

function handler(item, objEl, event) {
	if (event.target.classList.contains('likeBtn')) {
		objEl.addLike = objEl.addLike.bind(objEl);
		objEl.addLike();
		
		item.children[4].innerText = objEl.likes;
	}

}

