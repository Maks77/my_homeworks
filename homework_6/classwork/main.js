window.onload = function () {
	mainObj.render();
}

let mainObj = {
	msgArray: [],

	deleteMsgObj: function (id) {
		let index;
		this.msgArray.forEach((el, ind)=>{
			if (el.id == id) {
				return index = ind;
			}
		});

		if (index === undefined) return;
		this.msgArray.splice(index, 1);
		this.render();
	},

	addObj: function (element) {
		msgArray.push(element);
	},

	render: function (element) {
		let target = document.getElementById('commentsField');
			target.innerHTML = null;
		this.msgArray.map(el=>{
			let items = createComment(el);
			// console.log(items);
			target.appendChild(items)
		})
	},

	findElem: function ( id ) {
		let item;
		this.msgArray.forEach(el=>{
			if (el.id == id) {
				// console.log('element is finding', el);
				item = el;
				return;
			}
		})
		return item;
	},
}


class Message {
	constructor(id, author, text, date) {
		this.id = id;
		this.author = author;
		this.text = text;
		this.date = date;
		this.answers = [];
		mainObj.msgArray.push(this);
	}


	AnswerMessage(parent, id, text) {
		this.answers.push(new Answer(answIdCounter, this.author, text, this.date, this.id));
	}

}

class Answer extends Message {
	constructor(id, author, text, date, parentId) {
		super(id, author, text, date);
		this.parentId = parentId;
		answIdCounter++;
	}
}

let form = document.forms.message_form;
let idCounter = 0;
let answIdCounter = 0;

form.addEventListener('submit', function (e) {
	e.preventDefault();

	// console.dir(form);
	// console.dir(form.author.value);

	let pickedAuthor = form.author.value;
	let messageText = form.message.value;
	let myDate = createCurrentDate();


	let currentMSG = new Message(idCounter, pickedAuthor, messageText, myDate);
	// console.log("myMessage", currentMSG);
	idCounter++;
	form.message.value = null;
	mainObj.render();
	form.message.focus();
})



function createCurrentDate() {
	let date = new Date();
	let day = date.getDate();
	let month = date.getMonth();
	let year = date.getFullYear();

	let mDate = day + '/' + month + '/' + year;
	return mDate;
}





function createComment( msgObj ) {
	let commentsItem = document.createElement('li');
		commentsItem.setAttribute('data-id', msgObj.id);
		commentsItem.style.borderBottom = '1px solid grey'; 
		commentsItem.style.paddingBottom = '5px'; 

	let date = document.createElement('div');
		date.className = 'message__date';
		date.innerText = msgObj.date;

	let author = document.createElement('div');
		author.className = 'message__author';
		author.innerText = msgObj.author;

	let msgText = document.createElement('div');
		msgText.className = 'message__text';
		msgText.innerText = 'message text: ' + msgObj.text;
		msgText.style.color = 'red';

	let btn_skip = document.createElement('button');
		btn_skip.innerText = 'Skip';
		btn_skip.addEventListener('click', e=>{
			let itemId = btn_skip.parentElement.dataset.id;
			mainObj.deleteMsgObj(itemId);
		});
	let btn_answer = document.createElement('button');
		btn_answer.innerText = 'Answer';
		btn_answer.addEventListener('click', event=> {
			let parent = btn_answer.parentElement;
			if (btn_answer.parentElement.querySelector('.inputComment')) {
				// console.log('is null');
				return;
			};
			let item = createAnswerNode();
			parent.appendChild(item);
			// console.log('item', item);
			item.querySelector('textarea').focus();
		})

	let answerField = document.createElement('ul');
		answerField.className = 'answerField';
		// console.log('msgObj', msgObj);
		if (msgObj.answers !== undefined) {
			msgObj.answers.forEach(answItem=>{
				// console.log('answItem', answItem);
				let answBlock = document.createElement('li');
					answBlock.style.margin = '10px 0';
					answBlock.style.marginLeft = '30px';
					answBlock.innerText = answItem.text;
				answerField.appendChild(answBlock);
			})
		}

		commentsItem.appendChild(date);
		commentsItem.appendChild(author);
		commentsItem.appendChild(msgText);
		commentsItem.appendChild(btn_skip);
		commentsItem.appendChild(btn_answer);
		commentsItem.appendChild(answerField);

	return commentsItem;
	
}

function createAnswerNode() {

	let text = document.createElement('textarea');
		text.style.display = 'block';

	let btnSendAnsw = document.createElement('button');
		btnSendAnsw.className = 'btnSendAnsw';
		btnSendAnsw.innerText = 'Send';
		

		btnSendAnsw.addEventListener('click', event=>{
			let text = btnSendAnsw.parentElement.querySelector('textarea').value;
			let parID = btnSendAnsw.parentElement.parentElement.dataset.id;

			// console.log('id', parID);
			let currentParentObj = mainObj.findElem(parID);
				currentParentObj.AnswerMessage(this, parID, text);

			
			// console.log(mainObj.msgArray);
			mainObj.render();

		})

	let target = document.createElement('div');
		target.style.marginLeft = '20px';
		target.style.marginTop = '20px';
		target.className = 'inputComment';
		target.appendChild(text);
		target.appendChild(btnSendAnsw);

		return target;
}

window.addEventListener('keydown', event =>{
	// console.log(event.keyCode);
	if (event.keyCode === 13) {
		console.log(mainObj.msgArray);
	}
})